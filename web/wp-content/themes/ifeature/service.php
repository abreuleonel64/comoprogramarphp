<?php
/*
 Template Name: Service
*/
header('Content-Type: application/json');
$id = (isset($_GET['id']) ? $_GET['id'] : null);
$limit = (isset($_GET['limit']) ? $_GET['limit'] : -1);
$service = new Service();

echo json_encode($service->findPosts($id, $limit), JSON_HEX_TAG);