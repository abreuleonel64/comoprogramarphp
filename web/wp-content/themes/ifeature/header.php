<?php 

/*
	Header
	Authors: Tyler Cunningham, Trent Lapinski
	Creates the theme header. 
	Copyright (C) 2011 CyberChimps
	Version 2.0
*/

/* Call globals. */	

	global $themename, $themeslug, $options;
	
/* End globals. */
	
?>
	<?php synapse_head_tag(); ?>

<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?> <!-- wp_enqueue_script( 'comment-reply' );-->
<?php wp_head(); ?> <!-- wp_head();-->
<script type="text/javascript" src="<?php bloginfo('url'); ?>/wp-content/themes/ifeature/js/functions.js"> </script>
</head><!-- closing head tag-->

<!--Google Analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-30909636-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<!-- Begin @synapse after_head_tag hook content-->
	<?php synapse_after_head_tag(); ?>
<!-- End @synapse after_head_tag hook content-->
	
<!-- Begin @synapse before_header hook  content-->
	<?php synapse_before_header(); ?> 
<!-- End @synapse before_header hook content -->
			
<header>		
	<div id="login-register">
		<a href="<?= bloginfo('url') ?>/wp-login.php">Login </a> | <a href="<?= bloginfo('url') ?>/wp-login.php?action=register" >Registre-se</a>
	</div>
	<?php
		foreach(explode(",", $options->get('header_section_order')) as $fn) {
			
			if(function_exists($fn)) {
				call_user_func_array($fn, array());
			}
		}
	?>
</header>

<!-- Begin @synapse after_header hook -->
	<?php synapse_after_header(); ?> 
<!-- End @synapse after_header hook -->


