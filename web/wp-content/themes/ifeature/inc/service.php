<?php
class Service {
	private $response = array(
				'service' => array(
					'name' => 'Como Programar PHP',
					'url' => 'http://www.comoprogramarphp.com.br'
 				),
			); 
	
	
	public function findPosts($id = null, $limit = -1) {
		global $post;

		$result = array();
		$count = 0;
		$paramns['posts_per_page'] = $limit;
		$paramns['status'] = 'publish';
		$paramns['orderby'] = 'ID';
		$paramns['order'] = 'DESC';
		
		if(!is_null($id)) $paramns['p'] = $id;
		
		$query = new WP_Query($paramns);
		if ($query->have_posts()) {
			while($query->have_posts())	{
				$query->the_post();
				$postimage 	= get_post_meta($post->ID, 'slider_image' , true);
				$result[$count]['id'] = $post->ID;
				$result[$count]['title'] = $post->post_title; 
				$result[$count]['content'] = $post->post_content;
				$result[$count]['image'] = $postimage;
				$result[$count]['date'] = $post->post_date;
				$result[$count]['modified'] = $post->post_modified;
				$result[$count]['author'] = array('id' => $post->post_author, 'name' => get_author_name($post->post_author), 'email' => get_the_author_meta('user_email', $post->post_author));
				$result[$count]['link'] = get_bloginfo('url') . '/' . $post->post_name;
				$result[$count]['parent'] = $post->post_parent;
				$result[$count]['comments'] = get_comments(array('post_id' => $post->ID, 'orderby' => 'id', 'order' => 'ASC'));
				$count++;
			}		
		}
		wp_reset_postdata();
		
		$this->response['data'] = $result;
		return $this->response;
	}
}